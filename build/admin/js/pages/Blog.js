"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Blog = function (_React$Component) {
	_inherits(Blog, _React$Component);

	function Blog(props) {
		_classCallCheck(this, Blog);

		var _this = _possibleConstructorReturn(this, (Blog.__proto__ || Object.getPrototypeOf(Blog)).call(this, props));

		_this.getInitialState = _this.getInitialState.bind(_this);
		_this.getBlogPosts = _this.getBlogPosts.bind(_this);
		_this.showForm = _this.showForm.bind(_this);
		_this.closeForm = _this.closeForm.bind(_this);
		_this.change = _this.change.bind(_this);
		_this.save = _this.save.bind(_this);
		_this.cancel = _this.cancel.bind(_this);
		_this.deleteBlogPost = _this.deleteBlogPost.bind(_this);
		_this.changeBlogPost = _this.changeBlogPost.bind(_this);

		// Initialize state
		_this.state = _this.getInitialState();
		_this.getBlogPosts();
		return _this;
	}

	_createClass(Blog, [{
		key: "getInitialState",
		value: function getInitialState() {
			return {
				blogPost: {
					id: "0",
					title: "",
					url_title: "",
					draft: true,
					intro: "",
					post: "",
					tags: ""
				},
				blogPosts: []
			};
		}
	}, {
		key: "getBlogPosts",
		value: function getBlogPosts() {
			var self = this;
			$.ajax({
				url: "/api/blog",
				dataType: "json"
			}).done(function (response) {
				if (response.status == "Success") {
					var blogPosts = response.data;
					self.setState({ blogPosts: blogPosts });
				} else {
					console.log(response.status, ": ", response.message);
				}
			}).fail(function (jqXhr) {
				console.log("Failed to get blog posts", jqXhr);
			});
		}
	}, {
		key: "showForm",
		value: function showForm() {
			this.setState(function (prevState) {
				return {
					showForm: true
				};
			});
		}
	}, {
		key: "closeForm",
		value: function closeForm() {
			this.setState(function (prevState) {
				return {
					showForm: false
				};
			});
			this.getBlogPosts();
		}
	}, {
		key: "changeBlogPost",
		value: function changeBlogPost(blogPost) {
			this.showForm();
			this.setState({ blogPost: blogPost });
		}
	}, {
		key: "change",
		value: function change(event) {
			var name = event.target.name;
			var value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

			var blogPost = this.state.blogPost;
			blogPost[name] = value;

			this.setState({ blogPost: blogPost });
		}
	}, {
		key: "deleteBlogPost",
		value: function deleteBlogPost(event) {
			event.preventDefault();
			var self = this;
			var blogPost = this.state.blogPost;

			$.ajax({
				type: 'DELETE',
				url: '/api/blog/delete/' + blogPost.id
			}).done(function (data) {
				self.setState(self.getInitialState());
				self.closeForm();
			}).fail(function (jqXhr) {
				console.log('Failed to delete blog post: ', jqXhr);
			});
		}
	}, {
		key: "save",
		value: function save(event) {
			event.preventDefault();
			var self = this;
			var blogPost = this.state.blogPost;
			var saveURL = blogPost.id == 0 ? "/api/blog/create" : "/api/blog/update/" + blogPost.id;

			$.ajax({
				type: 'POST',
				url: saveURL,
				data: JSON.stringify(blogPost)
			}).done(function (data) {
				self.setState(self.getInitialState());
				self.closeForm();
			}).fail(function (jqXhr) {
				console.log('Failed to save blog post: ', jqXhr);
			});
		}
	}, {
		key: "cancel",
		value: function cancel(event) {
			event.preventDefault();
			this.setState(this.getInitialState());
			this.closeForm();
		}
	}, {
		key: "render",
		value: function render() {
			var _this2 = this;

			var formClasses = this.state.showForm ? "panel-form show-panel" : "panel-form hide-panel";
			var listClasses = !this.state.showForm ? "panel-list show-panel" : "panel-list hide-panel";
			var blogPost = this.state.blogPost;
			var blogPosts = this.state.blogPosts;

			var blogPostList = blogPosts.length === 0 ? null : blogPosts.map(function (blogPost, index) {
				return _react2.default.createElement(
					"li",
					{ key: index, className: "panel blog" },
					_react2.default.createElement("div", { className: "panel-header" }),
					_react2.default.createElement(
						"div",
						{ className: "panel-content" },
						_react2.default.createElement(
							"h3",
							{ onClick: function onClick() {
									return _this2.changeBlogPost(blogPost);
								} },
							blogPost.title
						),
						_react2.default.createElement("div", { dangerouslySetInnerHTML: { __html: blogPost.intro } })
					)
				);
			});

			return _react2.default.createElement(
				"div",
				{ id: "blog" },
				_react2.default.createElement(
					"header",
					{ className: "page" },
					_react2.default.createElement(
						"div",
						{ className: "container-fluid" },
						_react2.default.createElement(
							"div",
							{ className: "page-title" },
							_react2.default.createElement(
								"h2",
								null,
								_react2.default.createElement("i", { className: "fa fa-id-card-o", "aria-hidden": "true" }),
								"Blog"
							)
						),
						_react2.default.createElement(
							"div",
							{ className: "page-actions" },
							_react2.default.createElement(
								"div",
								{ className: "page-action", onClick: this.showForm },
								_react2.default.createElement("i", { className: "fa fa-plus" })
							)
						)
					)
				),
				_react2.default.createElement(
					"div",
					{ className: "main" },
					_react2.default.createElement(
						"div",
						{ className: "container-fluid" },
						_react2.default.createElement(
							"div",
							{ id: "blog-form", className: formClasses },
							_react2.default.createElement(
								"div",
								{ className: "panel blog-form" },
								_react2.default.createElement(
									"div",
									{ className: "panel-header" },
									_react2.default.createElement(
										"div",
										{ className: "panel-action-group" },
										_react2.default.createElement(
											"div",
											{ className: "panel-action", onClick: this.cancel },
											_react2.default.createElement("i", { className: "fa fa-remove" })
										)
									)
								),
								_react2.default.createElement(
									"div",
									{ className: "panel-content" },
									_react2.default.createElement(
										"form",
										null,
										_react2.default.createElement(
											"div",
											{ className: "" },
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Title:"
												),
												_react2.default.createElement("input", { type: "text", name: "title", value: this.state.blogPost.title, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element intro" },
												_react2.default.createElement(
													"label",
													null,
													"Intro:"
												),
												_react2.default.createElement("textarea", { name: "intro", value: this.state.blogPost.intro, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element post" },
												_react2.default.createElement(
													"label",
													{ className: "textarea" },
													"Post:"
												),
												_react2.default.createElement("textarea", { name: "post", value: this.state.blogPost.post, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Draft:"
												),
												_react2.default.createElement("input", { type: "checkbox", name: "draft", checked: this.state.blogPost.draft, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Tags:"
												),
												_react2.default.createElement("input", { type: "text", name: "tags", value: this.state.blogPost.tags, onChange: this.change })
											)
										),
										_react2.default.createElement(
											"div",
											{ className: "form-element button-group" },
											_react2.default.createElement(
												"button",
												{ onClick: this.save },
												_react2.default.createElement("i", { className: "fa fa-cloud-upload" }),
												_react2.default.createElement(
													"span",
													{ className: "title" },
													"Save"
												)
											),
											_react2.default.createElement(
												"button",
												{ onClick: this.deleteBlogPost },
												_react2.default.createElement("i", { className: "fa fa-times-circle" }),
												_react2.default.createElement(
													"span",
													{ className: "title" },
													"Delete"
												)
											),
											_react2.default.createElement(
												"button",
												{ onClick: this.cancel },
												_react2.default.createElement("i", { className: "fa fa-close" }),
												_react2.default.createElement(
													"span",
													{ className: "title" },
													"Cancel"
												)
											)
										)
									)
								),
								_react2.default.createElement("div", { className: "panel-footer" })
							)
						),
						_react2.default.createElement(
							"div",
							{ id: "blog-list", className: listClasses },
							_react2.default.createElement(
								"ul",
								{ className: "blog-posts" },
								blogPostList
							)
						)
					)
				)
			);
		}
	}]);

	return Blog;
}(_react2.default.Component);

exports.default = Blog;