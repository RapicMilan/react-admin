"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require("react-router");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Projects = function (_React$Component) {
	_inherits(Projects, _React$Component);

	function Projects(props) {
		_classCallCheck(this, Projects);

		var _this = _possibleConstructorReturn(this, (Projects.__proto__ || Object.getPrototypeOf(Projects)).call(this, props));

		_this.getInitialState = _this.getInitialState.bind(_this);
		_this.showForm = _this.showForm.bind(_this);
		_this.closeForm = _this.closeForm.bind(_this);
		_this.change = _this.change.bind(_this);
		_this.save = _this.save.bind(_this);
		_this.cancel = _this.cancel.bind(_this);
		_this.addImages = _this.addImages.bind(_this);
		_this.changeImages = _this.changeImages.bind(_this);
		_this.removeImage = _this.removeImage.bind(_this);
		_this.getProjects = _this.getProjects.bind(_this);
		_this.deleteProject = _this.deleteProject.bind(_this);
		_this.changeProject = _this.changeProject.bind(_this);

		// Initialize state
		_this.state = _this.getInitialState();
		_this.getProjects();
		return _this;
	}

	_createClass(Projects, [{
		key: "getInitialState",
		value: function getInitialState() {
			return {
				showForm: false,
				project: {
					id: "0",
					title: "",
					website: "",
					status: "In progress",
					url_title: "",
					description: "",
					technologies: "",
					images: []
				},
				projects: []
			};
		}
	}, {
		key: "getProjects",
		value: function getProjects() {
			var self = this;
			$.ajax({
				url: "/api/projects",
				dataType: 'json'
			}).done(function (response) {
				if (response.status == "Success") {
					var projects = response.data;
					for (var i = 0; i < projects.length; i++) {
						if (projects[i].images == null) {
							projects[i].images = [];
						}
					}

					self.setState({ projects: projects });
				} else {
					console.log(response.status, ": ", response.message);
				}
			}).fail(function (jqXhr) {
				console.log('Failed to get projects', jqXhr);
			});
		}
	}, {
		key: "showForm",
		value: function showForm() {
			this.setState(function (prevState) {
				return {
					showForm: true
				};
			});
		}
	}, {
		key: "closeForm",
		value: function closeForm() {
			this.setState(function (prevState) {
				return {
					showForm: false
				};
			});
			this.getProjects();
		}
	}, {
		key: "changeProject",
		value: function changeProject(project) {
			this.showForm();
			this.setState({ project: project });
		}
	}, {
		key: "addImages",
		value: function addImages(event) {
			event.preventDefault();

			var project = this.state.project;
			var image = {
				image: "",
				thumbnail: ""
			};

			project.images.push(image);
			this.setState({ project: project });
		}
	}, {
		key: "change",
		value: function change(event) {
			var name = event.target.name;
			var value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

			var project = this.state.project;
			project[name] = value;

			this.setState({ project: project });
		}
	}, {
		key: "changeImages",
		value: function changeImages(event) {
			var name = event.target.name;
			var index = event.target.getAttribute("data-index");
			var project = this.state.project;

			switch (name) {
				case "image":
					project.images[index].image = event.target.value;
					break;
				case "thumbnail":
					project.images[index].thumbnail = event.target.value;
					break;
			}

			this.setState({ project: project });
		}
	}, {
		key: "removeImage",
		value: function removeImage(index) {
			var project = this.state.project;
			if (index > -1) {
				project.images.splice(index, 1);
			}
			this.setState({ project: project });
		}
	}, {
		key: "deleteProject",
		value: function deleteProject(event) {
			event.preventDefault();
			var self = this;
			var project = this.state.project;

			$.ajax({
				type: 'DELETE',
				url: '/api/projects/delete/' + project.id
			}).done(function (data) {
				self.setState(self.getInitialState());
				self.closeForm();
			}).fail(function (jqXhr) {
				console.log('Failed to delete project:', jqXhr);
			});
		}
	}, {
		key: "save",
		value: function save(event) {
			event.preventDefault();
			var self = this;
			var project = this.state.project;
			var saveURL = project.id == 0 ? "/api/projects/create" : "/api/projects/update/" + project.id;

			$.ajax({
				type: 'POST',
				url: saveURL,
				data: JSON.stringify(project)
			}).done(function (data) {
				self.setState(self.getInitialState());
				self.closeForm();
			}).fail(function (jqXhr) {
				console.log('Failed to save project: ', jqXhr);
			});
		}
	}, {
		key: "cancel",
		value: function cancel(event) {
			event.preventDefault();
			this.setState(this.getInitialState());
			this.closeForm();
		}
	}, {
		key: "render",
		value: function render() {
			var _this2 = this;

			var formClasses = this.state.showForm ? "panel-form show-panel" : "panel-form hide-panel";
			var listClasses = !this.state.showForm ? "panel-list show-panel" : "panel-list hide-panel";
			var project = this.state.project;
			var projects = this.state.projects;

			var projectImages = project.images.map(function (image, index) {
				return _react2.default.createElement(
					"li",
					{ key: index },
					_react2.default.createElement(
						"div",
						{ className: "panel-header" },
						_react2.default.createElement(
							"div",
							{ className: "panel-action-group" },
							_react2.default.createElement(
								"div",
								{ className: "panel-action", onClick: function onClick() {
										return _this2.removeImage(index);
									} },
								_react2.default.createElement("i", { className: "fa fa-remove" })
							)
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "image-preview" },
						_react2.default.createElement("img", { src: image.thumbnail || "/admin/static/img/placeholder.png", className: "img-responsive" })
					),
					_react2.default.createElement(
						"div",
						{ className: "image-path" },
						_react2.default.createElement(
							"div",
							{ className: "form-element" },
							_react2.default.createElement(
								"label",
								null,
								"Image:"
							),
							_react2.default.createElement("input", { type: "text", name: "image", value: image.image, onChange: _this2.changeImages, "data-index": index })
						),
						_react2.default.createElement(
							"div",
							{ className: "form-element" },
							_react2.default.createElement(
								"label",
								null,
								"Thumbnail:"
							),
							_react2.default.createElement("input", { type: "text", name: "thumbnail", value: image.thumbnail, onChange: _this2.changeImages, "data-index": index })
						)
					)
				);
			});

			var projectList = projects.length === 0 ? null : projects.map(function (project, index) {
				return _react2.default.createElement(
					"li",
					{ key: index, className: "panel project" },
					_react2.default.createElement(
						"div",
						{ className: "panel-header" },
						_react2.default.createElement(
							"div",
							{ className: "panel-action-group" },
							_react2.default.createElement(
								"div",
								{ className: "panel-action", onClick: function onClick() {
										return _this2.changeProject(project);
									} },
								_react2.default.createElement("i", { className: "fa fa-pencil-square-o" })
							)
						)
					),
					_react2.default.createElement(
						"div",
						{ className: "panel-content" },
						_react2.default.createElement(
							"div",
							{ className: "project-details" },
							_react2.default.createElement(
								"h3",
								{ onClick: function onClick() {
										return _this2.changeProject(project);
									} },
								project.title
							),
							_react2.default.createElement(
								"p",
								null,
								project.website
							),
							_react2.default.createElement(
								"p",
								null,
								"Technologies: ",
								project.technologies
							),
							_react2.default.createElement(
								"p",
								null,
								project.description
							)
						),
						_react2.default.createElement(
							"div",
							{ className: "project-images" },
							_react2.default.createElement(
								"ul",
								null,
								project.images.map(function (image, index) {
									return _react2.default.createElement(
										"li",
										{ key: index },
										_react2.default.createElement("img", { src: image.thumbnail, className: "img-responsive" })
									);
								})
							)
						)
					)
				);
			});

			return _react2.default.createElement(
				"div",
				{ id: "projects" },
				_react2.default.createElement(
					"header",
					{ className: "page" },
					_react2.default.createElement(
						"div",
						{ className: "container-fluid" },
						_react2.default.createElement(
							"div",
							{ className: "page-title" },
							_react2.default.createElement(
								"h2",
								null,
								_react2.default.createElement("i", { className: "fa fa-area-chart", "aria-hidden": "true" }),
								"Projects"
							)
						),
						_react2.default.createElement(
							"div",
							{ className: "page-actions" },
							_react2.default.createElement(
								"div",
								{ className: "page-action", onClick: this.showForm },
								_react2.default.createElement("i", { className: "fa fa-plus" })
							)
						)
					)
				),
				_react2.default.createElement(
					"div",
					{ className: "main" },
					_react2.default.createElement(
						"div",
						{ className: "container-fluid" },
						_react2.default.createElement(
							"div",
							{ id: "project-form", className: formClasses },
							_react2.default.createElement(
								"div",
								{ className: "panel project-form" },
								_react2.default.createElement(
									"div",
									{ className: "panel-header" },
									_react2.default.createElement(
										"div",
										{ className: "panel-action-group" },
										_react2.default.createElement(
											"div",
											{ className: "panel-action", onClick: this.cancel },
											_react2.default.createElement("i", { className: "fa fa-remove" })
										)
									)
								),
								_react2.default.createElement(
									"div",
									{ className: "panel-content" },
									_react2.default.createElement(
										"form",
										null,
										_react2.default.createElement(
											"div",
											{ className: "" },
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Title:"
												),
												_react2.default.createElement("input", { type: "text", name: "title", value: this.state.project.title, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Website:"
												),
												_react2.default.createElement("input", { type: "text", name: "website", value: this.state.project.website, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Status:"
												),
												_react2.default.createElement(
													"select",
													{ name: "status", value: this.state.project.status, onChange: this.change },
													_react2.default.createElement(
														"option",
														{ value: "In progress" },
														"Work in progress"
													),
													_react2.default.createElement(
														"option",
														{ value: "Production ready" },
														"Finished"
													)
												)
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													null,
													"Technologies:"
												),
												_react2.default.createElement("input", { type: "text", name: "technologies", value: this.state.project.technologies, onChange: this.change })
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"label",
													{ className: "textarea" },
													"Description:"
												),
												_react2.default.createElement("textarea", { name: "description", value: this.state.project.description, onChange: this.change })
											)
										),
										_react2.default.createElement(
											"div",
											{ className: "" },
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"button",
													{ onClick: this.addImages },
													_react2.default.createElement("i", { className: "fa fa-picture-o" }),
													_react2.default.createElement(
														"span",
														{ className: "title" },
														"Add Images"
													)
												)
											),
											_react2.default.createElement(
												"div",
												{ className: "form-element" },
												_react2.default.createElement(
													"ul",
													{ className: "images" },
													projectImages
												)
											)
										),
										_react2.default.createElement(
											"div",
											{ className: "form-element button-group" },
											_react2.default.createElement(
												"button",
												{ onClick: this.save },
												_react2.default.createElement("i", { className: "fa fa-cloud-upload" }),
												_react2.default.createElement(
													"span",
													{ className: "title" },
													"Save"
												)
											),
											_react2.default.createElement(
												"button",
												{ onClick: this.deleteProject },
												_react2.default.createElement("i", { className: "fa fa-times-circle" }),
												_react2.default.createElement(
													"span",
													{ className: "title" },
													"Delete"
												)
											),
											_react2.default.createElement(
												"button",
												{ onClick: this.cancel },
												_react2.default.createElement("i", { className: "fa fa-close" }),
												_react2.default.createElement(
													"span",
													{ className: "title" },
													"Cancel"
												)
											)
										)
									)
								),
								_react2.default.createElement("div", { className: "panel-footer" })
							)
						),
						_react2.default.createElement(
							"div",
							{ id: "project-list", className: listClasses },
							_react2.default.createElement(
								"ul",
								{ className: "projects" },
								projectList
							)
						)
					)
				)
			);
		}
	}]);

	return Projects;
}(_react2.default.Component);

exports.default = Projects;