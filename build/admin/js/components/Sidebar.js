'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRouter = require('react-router');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Sidebar = function (_React$Component) {
	_inherits(Sidebar, _React$Component);

	function Sidebar(props) {
		_classCallCheck(this, Sidebar);

		var _this = _possibleConstructorReturn(this, (Sidebar.__proto__ || Object.getPrototypeOf(Sidebar)).call(this, props));

		_this.state = {
			settingsSubmenuOpen: false
		};
		_this.settingsSubmenuToggle = _this.settingsSubmenuToggle.bind(_this);
		return _this;
	}

	_createClass(Sidebar, [{
		key: 'settingsSubmenuToggle',
		value: function settingsSubmenuToggle() {
			this.setState(function (prevState) {
				return {
					settingsSubmenuOpen: !prevState.settingsSubmenuOpen
				};
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var sidebarClass = 'sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures';
			var nanoClass = this.props.isOpen ? 'nano has-scrollbar' : 'nano';

			var settingsSidebarCollapseIconClass = this.state.projectsSubmenuOpen ? "sidebar-collapse-icon fa fa-angle-up" : "sidebar-collapse-icon fa fa-angle-down";
			var settingsSubmenuOpenClass = this.state.settingsSubmenuOpen ? "open" : "";
			var settingsSubmenuShowClass = this.state.settingsSubmenuOpen ? "show" : "hide";

			return _react2.default.createElement(
				'div',
				{ className: sidebarClass },
				_react2.default.createElement(
					'div',
					{ className: nanoClass },
					_react2.default.createElement(
						'div',
						{ className: 'nano-content' },
						_react2.default.createElement(
							'ul',
							null,
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: "/admin/dashboard", activeClassName: 'active' },
									_react2.default.createElement('i', { className: 'fa fa-dashboard', 'aria-hidden': 'true' }),
									'Dashboard'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: '/admin/projects', activeClassName: 'active' },
									_react2.default.createElement('i', { className: 'fa fa-area-chart', 'aria-hidden': 'true' }),
									'Projects'
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: "/admin/blog", activeClassName: 'active' },
									_react2.default.createElement('i', { className: 'fa fa-id-card-o', 'aria-hidden': 'true' }),
									'Blog'
								)
							),
							_react2.default.createElement(
								'li',
								{ className: settingsSubmenuOpenClass },
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: '/admin', activeClassName: '', className: 'sidebar-sub-toggle', onClick: this.settingsSubmenuToggle },
									_react2.default.createElement('i', { className: 'fa fa-gears', 'aria-hidden': 'true' }),
									'Settings',
									_react2.default.createElement('span', { className: settingsSidebarCollapseIconClass, 'aria-hidden': 'true' })
								),
								_react2.default.createElement(
									'ul',
									{ className: settingsSubmenuShowClass },
									_react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											_reactRouter.Link,
											{ to: '/admin', activeClassName: 'active' },
											'Settings 1'
										)
									),
									_react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											_reactRouter.Link,
											{ to: '/admin', activeClassName: 'active' },
											'Settings 2'
										)
									),
									_react2.default.createElement(
										'li',
										null,
										_react2.default.createElement(
											_reactRouter.Link,
											{ to: '/admin', activeClassName: 'active' },
											'Settings 3'
										)
									)
								)
							),
							_react2.default.createElement(
								'li',
								null,
								_react2.default.createElement(
									_reactRouter.Link,
									{ to: "/admin", activeClassName: '' },
									_react2.default.createElement('i', { className: 'fa fa-users', 'aria-hidden': 'true' }),
									'Users'
								)
							)
						)
					)
				)
			);
		}
	}]);

	return Sidebar;
}(_react2.default.Component);

exports.default = Sidebar;