'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Header = require('./Header');

var _Header2 = _interopRequireDefault(_Header);

var _Sidebar = require('./Sidebar');

var _Sidebar2 = _interopRequireDefault(_Sidebar);

var _Content = require('./Content');

var _Content2 = _interopRequireDefault(_Content);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Shell = function (_React$Component) {
	_inherits(Shell, _React$Component);

	function Shell(props) {
		_classCallCheck(this, Shell);

		var _this = _possibleConstructorReturn(this, (Shell.__proto__ || Object.getPrototypeOf(Shell)).call(this, props));

		_this.state = {
			sidebarOpen: true
		};
		_this.handleViewSidebar = _this.handleViewSidebar.bind(_this);
		return _this;
	}

	_createClass(Shell, [{
		key: 'handleViewSidebar',
		value: function handleViewSidebar() {
			//this.setState({sidebarOpen: !this.state.sidebarOpen});
			this.setState(function (prevState) {
				return {
					sidebarOpen: !prevState.sidebarOpen
				};
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var shellClass = this.state.sidebarOpen ? '' : 'sidebar-hide';
			return _react2.default.createElement(
				'div',
				{ className: shellClass },
				_react2.default.createElement(_Header2.default, { isOpen: this.state.sidebarOpen, onClick: this.handleViewSidebar }),
				_react2.default.createElement(_Sidebar2.default, { isOpen: this.state.sidebarOpen, toggleSidebar: this.handleViewSidebar }),
				_react2.default.createElement(_Content2.default, { isOpen: this.state.sidebarOpen, content: this.props.children })
			);
		}
	}]);

	return Shell;
}(_react2.default.Component);

exports.default = Shell;