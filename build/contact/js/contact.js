'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
	_inherits(App, _React$Component);

	function App(props) {
		_classCallCheck(this, App);

		var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this));

		_this.getInitialState = _this.getInitialState.bind(_this);
		_this.change = _this.change.bind(_this);
		_this.sendMail = _this.sendMail.bind(_this);
		_this.validateForm = _this.validateForm.bind(_this);
		_this.validateEmail = _this.validateEmail.bind(_this);

		// Initialize state
		_this.state = _this.getInitialState();
		return _this;
	}

	_createClass(App, [{
		key: 'getInitialState',
		value: function getInitialState() {
			return {
				formValid: false,
				showNameErrorMessage: false,
				showEmailErrorMessage: false,
				showMessageErrorMessage: false,
				showSuccessFlashMessage: false,
				showFailedFlashMessage: false,
				failedMessage: "",
				email: {
					name: "",
					email: "",
					message: ""
				}
			};
		}
	}, {
		key: 'change',
		value: function change(event) {
			var name = event.target.name;
			var value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

			var email = this.state.email;
			email[name] = value;

			// perform validations and enable this.state.buttonDisabled

			this.setState({ email: email });
			this.validateForm();
		}
	}, {
		key: 'validateForm',
		value: function validateForm() {
			var email = this.state.email;
			var nameValid = false;
			var emailValid = false;
			var messageValid = false;
			var formValid = false;
			// name
			if (email.name.length > 3) {
				nameValid = true;
				this.setState({ showNameErrorMessage: false });
			} else {
				this.setState({ showNameErrorMessage: true });
			}

			// email
			if (email.email.length > 3 && this.validateEmail(this.state.email.email)) {
				emailValid = true;
				this.setState({ showEmailErrorMessage: false });
			} else {
				this.setState({ showEmailErrorMessage: true });
			}

			// message
			if (email.message.length > 20) {
				messageValid = true;
				this.setState({ showMessageErrorMessage: false });
			} else {
				this.setState({ showMessageErrorMessage: true });
			}

			formValid = nameValid && emailValid && messageValid;
			this.setState({ formValid: formValid });
		}
	}, {
		key: 'validateEmail',
		value: function validateEmail(email) {
			//var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; // Support unicode
			return re.test(email);
		}
	}, {
		key: 'sendMail',
		value: function sendMail(event) {
			event.preventDefault();
			var self = this;
			var email = this.state.email;
			var saveURL = "/api/send-mail";

			self.setState({ showSuccessFlashMessage: false });
			self.setState({ showFailedFlashMessage: false });

			$.ajax({
				type: 'POST',
				url: saveURL,
				data: JSON.stringify(email)
			}).done(function (response) {
				if (response.status == "Success") {
					// Display success flash message
					self.setState({ showSuccessFlashMessage: true });

					// Initialize state
					setTimeout(function () {
						self.setState(self.getInitialState());
					}.bind(this), 3000);
				} else {
					self.setState({ showFailedFlashMessage: true });
					self.setState({ failedMessage: response.message });
					setTimeout(function () {
						self.setState({ showFailedFlashMessage: false });
					}.bind(this), 3000);
				}

				// Display error flash message
			}).fail(function (response) {
				// Display error flash message
				self.setState({ showFailedFlashMessage: true });
				self.setState({ failedMessage: response.responseText });
				setTimeout(function () {
					self.setState({ showFailedFlashMessage: false });
				}.bind(this), 3000);
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var formButton = this.state.formValid === true ? _react2.default.createElement(
				'button',
				{ className: 'btn-callout text-center', onClick: this.sendMail },
				_react2.default.createElement(
					'div',
					{ className: 'text' },
					'Send message'
				),
				_react2.default.createElement(
					'div',
					{ className: 'icon' },
					_react2.default.createElement('span', { className: 'fa fa-paper-plane' })
				)
			) : _react2.default.createElement(
				'button',
				{ className: 'btn-callout text-center disabled', disabled: 'disabled', onClick: this.sendMail },
				_react2.default.createElement(
					'div',
					{ className: 'text' },
					'Send message'
				),
				_react2.default.createElement(
					'div',
					{ className: 'icon' },
					_react2.default.createElement('span', { className: 'fa fa-paper-plane' })
				)
			);

			var nameErrorMessageClass = this.state.showNameErrorMessage ? "error-message" : "error-message hide";
			var emailErrorMessageClass = this.state.showEmailErrorMessage ? "error-message" : "error-message hide";
			var messageErrorMessageClass = this.state.showMessageErrorMessage ? "error-message" : "error-message hide";

			var successConfirmation = !this.state.showSuccessFlashMessage ? "" : _react2.default.createElement(
				'div',
				{ className: 'confirmation-message success' },
				'Email successfully sent'
			);

			var failedConfirmation = !this.state.showFailedFlashMessage ? "" : _react2.default.createElement(
				'div',
				{ className: 'confirmation-message failed' },
				this.state.failedMessage
			);

			return _react2.default.createElement(
				'div',
				{ className: 'contact-form' },
				_react2.default.createElement(
					'div',
					{ className: 'confirmation' },
					successConfirmation,
					failedConfirmation
				),
				_react2.default.createElement(
					'form',
					{ role: 'form' },
					_react2.default.createElement(
						'div',
						{ className: 'form-group input' },
						_react2.default.createElement(
							'label',
							{ htmlFor: 'name' },
							'Name:'
						),
						_react2.default.createElement('input', { type: 'text', name: 'name', value: this.state.email.name, onChange: this.change, placeholder: 'Your name', autoComplete: 'off' }),
						_react2.default.createElement(
							'div',
							{ className: nameErrorMessageClass },
							'Name is required, and it must contain at least 3 characters.'
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'form-group input' },
						_react2.default.createElement(
							'label',
							{ htmlFor: 'email' },
							'Email:'
						),
						_react2.default.createElement('input', { type: 'text', name: 'email', value: this.state.email.email, onChange: this.change, placeholder: 'username@domain.com', autoComplete: 'off' }),
						_react2.default.createElement(
							'div',
							{ className: emailErrorMessageClass },
							'Email is required, must be valid, and it must contain at least 5 characters.'
						)
					),
					_react2.default.createElement(
						'div',
						{ className: 'form-group' },
						_react2.default.createElement(
							'label',
							{ htmlFor: 'message' },
							'Message:'
						),
						_react2.default.createElement('textarea', { name: 'message', value: this.state.email.message, onChange: this.change, placeholder: 'Tell me about yourself and about the project you want me to develop.' }),
						_react2.default.createElement(
							'div',
							{ className: messageErrorMessageClass },
							'Message is required, and it must contain at least 20 characters.'
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'form-button' },
					formButton
				)
			);
		}
	}]);

	return App;
}(_react2.default.Component);

_reactDom2.default.render(_react2.default.createElement(App, null), document.getElementById("contact-app"));