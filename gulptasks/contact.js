/** **************************************************************** **

    @Project:   Build Automation Script - Contact
	@Author:    Rapić Milan
	@Website:   www.rapicmilan.com

	Table contents
	----------------------
	01. Plugins
	02. Files
	03. Clean Task
	04. CSS Build Task
	05. JS Bild Task
	06. Main Task

	----------------------

** **************************************************************** **/



/** **************************************************************** **

	01. Plugins

** **************************************************************** **/

var gulp        = require('gulp');
var browserify  = require('browserify');
var source      = require('vinyl-source-stream');
var buffer      = require('vinyl-buffer');
var jshint      = require('gulp-jshint');
var concat      = require('gulp-concat');
var rename      = require('gulp-rename');
var uglify      = require('gulp-uglify');
var babel       = require('gulp-babel');
var eslint      = require('gulp-eslint');
var filter      = require('gulp-filter');
var uglify      = require('gulp-uglify');
var sass        = require('gulp-sass');
var cssnano     = require('gulp-cssnano');
var pump        = require('pump');
var del         = require('del');




/** **************************************************************** **

	02. Files

** **************************************************************** **/

var contactJS = {
	vendor: [
		// Use CDN
		//'vendor/js/react.min.js',
		//'vendor/js/react-dom.min.js'
	],
	source: [
		'app/contact/js/**/*.js',
		'app/contact/js/**/*.jsx'
	]
};

var contactCSS = {
	vendor: [
		// Use CDN
		//'vendor/css/normalize.min.css',
		//'vendor/css/font-awesome.min.css',
	],
	source: [
		'app/contact/css/**/*.css'
	]
};

var contactSASS = {
	vendor: [],
	source: [
		'app/contact/sass/**/*.scss'
	]
}



/** **************************************************************** **

	03. Clean Task

** **************************************************************** **/

gulp.task('contact-clean-js', function(){
	return del([
		'build/contact/js',
	  	'../src/frontend/static/js/contact.min.js'
	], {force:true});
});

gulp.task('contact-clean-css', function(){
	return del([
	  	'build/contact/css',
	  	'../src/frontend/static/css/contact.min.css'
	], {force:true});
});

gulp.task('contact-clean', ['contact-clean-css', 'contact-clean-js']);



/** **************************************************************** **

	04. CSS Build Task

** **************************************************************** **/

gulp.task('contact-build-css-vendor', ['contact-clean-css'], function() {
	if (contactCSS.vendor.length === 0) {
		return;
	}
	return gulp.src(contactCSS.vendor)
	.pipe(concat('vendor.min.css'))
	.pipe(gulp.dest('../src/frontend/static/css'));
});

gulp.task('contact-build-css', ['contact-clean-css'], function() {
	return gulp.src(contactCSS.source)
	.pipe(concat('contact-css.css'))
	.pipe(gulp.dest('build/contact/css'));
});

gulp.task('contact-build-sass', ['contact-clean-css'], function() {
	return gulp.src(contactSASS.source)
	.pipe(concat('contact-sass.css'))
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('build/contact/css'));
});

gulp.task('contact-minify-css', ['contact-clean-css','contact-build-css-vendor', 'contact-build-css', 'contact-build-sass'], function() {
	return gulp.src('build/contact/css/*.css')
	.pipe(concat('contact.css'))
	.pipe(cssnano())
	.pipe(rename('contact.min.css'))
	.pipe(gulp.dest('../src/frontend/static/css'));
});

gulp.task('contact-css', ['contact-clean-css', 'contact-build-css-vendor', 'contact-build-css', 'contact-build-sass', 'contact-minify-css']);


/** **************************************************************** **

	05. JS Bild Task

** **************************************************************** **/

gulp.task('contact-build-js-vendor', ['contact-clean-js'], function() {
	 if (contactJS.vendor.length === 0) {
		 return;
	 }
	 return gulp.src(contactJS.vendor)
	 .pipe(concat('vendor.min.js'))
	 .pipe(gulp.dest("../src/frontend/static/js"));
});

gulp.task('contact-build-application', ['contact-clean-js'], function() {
	 return gulp.src(contactJS.source)
		 .pipe(babel({
			 presets: ['es2015', 'react']
		 }))
		 .pipe(gulp.dest('build/contact/js'));
});

gulp.task('contact-commonjs-bundle', ['contact-clean-js', 'contact-build-application'], function(){
	return browserify(['build/contact/js/contact.js']).bundle()
		.pipe(source('contact.js'))
	 	.pipe(buffer())
	 	.pipe(uglify())
	 	.pipe(rename('contact.min.js'))
	 	.pipe(gulp.dest("../src/frontend/static/js"));
});

gulp.task('contact-commonjs-bundle-dev', ['contact-clean-js', 'contact-build-application'], function(){
	return browserify(['build/contact/js/contact.js']).bundle()
		.pipe(source('contact.js'))
	 	.pipe(buffer())
	 	//.pipe(uglify())
	 	.pipe(rename('contact.min.js'))
	 	.pipe(gulp.dest("../src/frontend/static/js"));
});

gulp.task('contact-js', ['contact-clean-js', 'contact-build-js-vendor', 'contact-build-application', 'contact-commonjs-bundle']);



/** **************************************************************** **

	06. Main

** **************************************************************** **/

gulp.task('build-contact', ['contact-js', 'contact-css']);





/** **************************************************************** **/
