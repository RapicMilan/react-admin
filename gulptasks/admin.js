/** **************************************************************** **

    @Project:   Build Automation Script - Admin
	@Author:    Rapić Milan
	@Website:   www.rapicmilan.com

	Table contents
	----------------------
	01. Plugins
	02. Files
	03. Clean Task
	04. CSS Build Task
	05. JS Bild Task
	06. Main Task

	----------------------

** **************************************************************** **/



/** **************************************************************** **

	01. Plugins

** **************************************************************** **/

var gulp        = require('gulp');
var browserify  = require('browserify');
var source      = require('vinyl-source-stream');
var buffer      = require('vinyl-buffer');
var jshint      = require('gulp-jshint');
var concat      = require('gulp-concat');
var rename      = require('gulp-rename');
var uglify      = require('gulp-uglify');
var babel       = require('gulp-babel');
var eslint      = require('gulp-eslint');
var filter      = require('gulp-filter');
var uglify      = require('gulp-uglify');
var sass        = require('gulp-sass');
var cssnano     = require('gulp-cssnano');
var pump        = require('pump');
var del         = require('del');




/** **************************************************************** **

	02. Files

** **************************************************************** **/

var adminJS = {
	vendor: [
		// Use CDN
		//'vendor/js/react.min.js',
		//'vendor/js/react-dom.min.js'
	],
	source: [
		'app/admin/js/**/*.js',
		'app/admin/js/**/*.jsx'
	]
};

var adminCSS = {
	vendor: [
		// Use CDN
		//'vendor/css/normalize.min.css',
		//'vendor/css/font-awesome.min.css',
	],
	source: [
		'app/admin/css/**/*.css'
	]
};

var adminSASS = {
	vendor: [],
	source: [
		'app/admin/sass/**/*.scss'
	]
}



/** **************************************************************** **

	03. Clean Task

** **************************************************************** **/

gulp.task('admin-clean-js', function(){
	return del([
		'build/admin/js',
	  	'../src/admin/static/js'
	], {force:true});
});

gulp.task('admin-clean-css', function(){
	return del([
	  	'build/admin/css',
	  	'../src/admin/static/css'
	], {force:true});
});

gulp.task('admin-clean', ['admin-clean-css', 'admin-clean-js']);



/** **************************************************************** **

	04. CSS Build Task

** **************************************************************** **/

gulp.task('admin-build-css-vendor', ['admin-clean-css'], function() {
	if (adminCSS.vendor.length === 0) {
		return;
	}
	return gulp.src(adminCSS.vendor)
	.pipe(concat('vendor.min.css'))
	.pipe(gulp.dest('../src/admin/static/css'));
});

gulp.task('admin-build-css', ['admin-clean-css'], function() {
	return gulp.src(adminCSS.source)
	.pipe(concat('admin-css.css'))
	.pipe(gulp.dest('build/admin/css'));
});

gulp.task('admin-build-sass', ['admin-clean-css'], function() {
	return gulp.src(adminSASS.source)
	.pipe(concat('admin-sass.css'))
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('build/admin/css'));
});

gulp.task('admin-minify-css', ['admin-clean-css','admin-build-css-vendor', 'admin-build-css', 'admin-build-sass'], function() {
	return gulp.src('build/admin/css/*.css')
	.pipe(concat('admin.css'))
	.pipe(cssnano())
	.pipe(rename('admin.min.css'))
	.pipe(gulp.dest('../src/admin/static/css'));
});

gulp.task('admin-css', ['admin-clean-css', 'admin-build-css-vendor', 'admin-build-css', 'admin-build-sass', 'admin-minify-css']);


/** **************************************************************** **

	05. JS Bild Task

** **************************************************************** **/

gulp.task('admin-build-js-vendor', ['admin-clean-js'], function() {
	 if (adminJS.vendor.length === 0) {
		 return;
	 }
	 return gulp.src(adminJS.vendor)
	 .pipe(concat('vendor.min.js'))
	 .pipe(gulp.dest("../src/admin/static/js"));
});

gulp.task('admin-build-application', ['admin-clean-js'], function() {
	 return gulp.src(adminJS.source)
		 .pipe(babel({
			 presets: ['es2015', 'react']
		 }))
		 .pipe(gulp.dest('build/admin/js'));
});

gulp.task('admin-commonjs-bundle', ['admin-clean-js', 'admin-build-application'], function(){
	return browserify(['build/admin/js/admin.js']).bundle()
		.pipe(source('admin.js'))
	 	.pipe(buffer())
	 	.pipe(uglify())
	 	.pipe(rename('admin.min.js'))
	 	.pipe(gulp.dest("../src/admin/static/js"));
});

gulp.task('admin-commonjs-bundle-dev', ['admin-clean-js', 'admin-build-application'], function(){
	return browserify(['build/admin/js/admin.js']).bundle()
		.pipe(source('admin.js'))
	 	.pipe(buffer())
	 	//.pipe(uglify())
	 	.pipe(rename('admin.min.js'))
	 	.pipe(gulp.dest("../src/admin/static/js"));
});

gulp.task('admin-js', ['admin-clean-js', 'admin-build-js-vendor', 'admin-build-application', 'admin-commonjs-bundle']);



/** **************************************************************** **

	06. Main

** **************************************************************** **/

gulp.task('build-admin', ['admin-js', 'admin-css']);





/** **************************************************************** **/
