import React from 'react'
import ReactDOM from 'react-dom'

class App extends React.Component {
	constructor(props) {
		super();

		this.getInitialState = this.getInitialState.bind(this);
		this.change = this.change.bind(this);
		this.sendMail = this.sendMail.bind(this);
		this.validateForm = this.validateForm.bind(this);
		this.validateEmail = this.validateEmail.bind(this);

		// Initialize state
		this.state = this.getInitialState();
	}

	getInitialState() {
		return {
			formValid: false,
			showNameErrorMessage: false,
			showEmailErrorMessage: false,
			showMessageErrorMessage: false,
			showSuccessFlashMessage: false,
			showFailedFlashMessage: false,
			failedMessage: "",
			email: {
				name: "",
				email: "",
				message: ""
			}
		};
	}

	change(event){
		const name = event.target.name;
		const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

		let email = this.state.email;
		email[name] = value;

		// perform validations and enable this.state.buttonDisabled

		this.setState({email: email});
		this.validateForm();
	}

	validateForm() {
		let email = this.state.email;
		let nameValid = false;
		let emailValid = false;
		let messageValid = false;
		let formValid = false;
		// name
		if (email.name.length > 3) {
			nameValid = true;
			this.setState({showNameErrorMessage: false});
		} else {
			this.setState({showNameErrorMessage: true});
		}

		// email
		if ((email.email.length > 3) && (this.validateEmail(this.state.email.email))) {
			emailValid = true;
			this.setState({showEmailErrorMessage: false});
		} else {
			this.setState({showEmailErrorMessage: true});
		}

		// message
		if (email.message.length > 20) {
			messageValid = true;
			this.setState({showMessageErrorMessage: false});
		} else {
			this.setState({showMessageErrorMessage: true});
		}

		formValid = nameValid && emailValid && messageValid;
		this.setState({formValid: formValid})
	}

	validateEmail(email) {
	    //var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;  // Support unicode
	    return re.test(email);
	}

	sendMail(event) {
		event.preventDefault();
		let self = this;
		let email = this.state.email;
		let saveURL = "/api/send-mail";

		self.setState({showSuccessFlashMessage: false});
		self.setState({showFailedFlashMessage: false});

		$.ajax({
	    	type: 'POST',
	    	url: saveURL,
	    	data: JSON.stringify(email)
	  	})
	  	.done(function(response) {
			if (response.status == "Success") {
				// Display success flash message
				self.setState({showSuccessFlashMessage: true});

				// Initialize state
				setTimeout(function() {
					self.setState(self.getInitialState());
				}.bind(this), 3000);
			} else {
				self.setState({showFailedFlashMessage: true});
				self.setState({failedMessage: response.message});
				setTimeout(function() {
					self.setState({showFailedFlashMessage: false});
				}.bind(this), 3000);
			}

			// Display error flash message
	  	})
	  	.fail(function(response) {
			// Display error flash message
			self.setState({showFailedFlashMessage: true});
			self.setState({failedMessage: response.responseText});
			setTimeout(function() {
				self.setState({showFailedFlashMessage: false});
			}.bind(this), 3000);
	  	});
	}

	render(){
		const formButton = this.state.formValid === true ?
		(
			<button  className="btn-callout text-center" onClick={this.sendMail}>
				<div className="text">Send message</div><div className="icon"><span className="fa fa-paper-plane"></span></div>
			</button>
		) :
		(
			<button  className="btn-callout text-center disabled" disabled="disabled" onClick={this.sendMail}>
				<div className="text">Send message</div><div className="icon"><span className="fa fa-paper-plane"></span></div>
			</button>
		)

		const nameErrorMessageClass = this.state.showNameErrorMessage ? "error-message" : "error-message hide";
		const emailErrorMessageClass = this.state.showEmailErrorMessage ? "error-message" : "error-message hide";
		const messageErrorMessageClass = this.state.showMessageErrorMessage ? "error-message" : "error-message hide";

		const successConfirmation = !this.state.showSuccessFlashMessage ? "" :
		(
			<div className="confirmation-message success">
				Email successfully sent
			</div>
		);

		const failedConfirmation = !this.state.showFailedFlashMessage ? "" :
		(
			<div className="confirmation-message failed">
				{this.state.failedMessage}
			</div>
		);

		return (
			<div className="contact-form">
				<div className="confirmation">
					{successConfirmation}
					{failedConfirmation}
				</div>
				<form role="form">
					<div className="form-group input">
						<label htmlFor="name">Name:</label>
						<input type="text" name="name" value={this.state.email.name} onChange={this.change} placeholder="Your name" autoComplete="off" />
						<div className={nameErrorMessageClass}>Name is required, and it must contain at least 3 characters.</div>
					</div>
					<div className="form-group input">
						<label htmlFor="email">Email:</label>
						<input type="text" name="email" value={this.state.email.email} onChange={this.change} placeholder="username@domain.com" autoComplete="off"  />
						<div className={emailErrorMessageClass}>Email is required, must be valid, and it must contain at least 5 characters.</div>
					</div>
					<div className="form-group">
						<label htmlFor="message">Message:</label>
						<textarea name="message" value={this.state.email.message} onChange={this.change} placeholder="Tell me about yourself and about the project you want me to develop."></textarea>
						<div className={messageErrorMessageClass}>Message is required, and it must contain at least 20 characters.</div>
					</div>
				</form>

				<div className="form-button">
					{formButton}
				</div>
			</div>
		)
	}
}

ReactDOM.render(<App />, document.getElementById("contact-app"))
