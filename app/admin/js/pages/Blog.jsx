import React from 'react'

class Blog extends React.Component {
	constructor(props) {
		super(props);

		this.getInitialState = this.getInitialState.bind(this);
		this.getBlogPosts = this.getBlogPosts.bind(this);
		this.showForm = this.showForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.change = this.change.bind(this);
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
		this.deleteBlogPost = this.deleteBlogPost.bind(this);
		this.changeBlogPost = this.changeBlogPost.bind(this);

		// Initialize state
		this.state = this.getInitialState();
		this.getBlogPosts();
	}

	getInitialState() {
		return {
			blogPost: {
				id: "0",
				title: "",
				url_title: "",
				draft: true,
				intro: "",
				post: "",
				tags: ""
			},
			blogPosts: []
		};
	}

	getBlogPosts() {
		var self = this;
		$.ajax({
			url: "/api/blog",
			dataType: "json"
		})
		.done(function(response){
			if (response.status == "Success") {
				let blogPosts = response.data;
				self.setState({blogPosts: blogPosts});
			} else {
				console.log(response.status, ": ", response.message);
			}
		})
		.fail(function(jqXhr){
			console.log("Failed to get blog posts", jqXhr);
		})
	}

	showForm() {
		this.setState((prevState) => ({
			showForm: true
		}));
	}

	closeForm() {
		this.setState((prevState) => ({
			showForm: false
		}));
		this.getBlogPosts();
	}

	changeBlogPost(blogPost) {
		this.showForm();
		this.setState({blogPost: blogPost});
	}

	change(event){
		const name = event.target.name;
		const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

		let blogPost = this.state.blogPost;
		blogPost[name] = value;

		this.setState({blogPost: blogPost});
	}

	deleteBlogPost(event) {
		event.preventDefault();
		let self = this;
		let blogPost = this.state.blogPost;

		$.ajax({
	    	type: 'DELETE',
	    	url: '/api/blog/delete/' + blogPost.id
	  	})
	  	.done(function(data) {
			self.setState(self.getInitialState());
		  	self.closeForm();
	  	})
	  	.fail(function(jqXhr) {
	    	console.log('Failed to delete blog post: ', jqXhr);
	  	});
	}

	save(event) {
		event.preventDefault();
		let self = this;
		let blogPost = this.state.blogPost;
		let saveURL = blogPost.id == 0 ? "/api/blog/create" : "/api/blog/update/" + blogPost.id;

		$.ajax({
	    	type: 'POST',
	    	url: saveURL,
	    	data: JSON.stringify(blogPost)
	  	})
	  	.done(function(data) {
			self.setState(self.getInitialState());
		  	self.closeForm();
	  	})
	  	.fail(function(jqXhr) {
	    	console.log('Failed to save blog post: ', jqXhr);
	  	});
	}

	cancel(event) {
		event.preventDefault();
		this.setState(this.getInitialState());
		this.closeForm();
	}

	render() {
		let formClasses = this.state.showForm ? "panel-form show-panel" : "panel-form hide-panel";
		let listClasses = !this.state.showForm ? "panel-list show-panel" : "panel-list hide-panel";
		let blogPost = this.state.blogPost;
		let blogPosts = this.state.blogPosts;

		const blogPostList = (blogPosts.length === 0) ? null : blogPosts.map((blogPost, index) =>
		<li key={index} className="panel blog">
			<div className="panel-header">
			</div>
			<div className="panel-content">
				<h3 onClick={() => this.changeBlogPost(blogPost)}>{blogPost.title}</h3>
				<div dangerouslySetInnerHTML={{__html: blogPost.intro}}></div>
			</div>
		</li>
		);

		return (
			<div id="blog">
				<header className="page">
					<div className="container-fluid">
						<div className="page-title">
							<h2>
								<i className="fa fa-id-card-o" aria-hidden="true"></i>
								Blog
							</h2>
						</div>
						<div className="page-actions">
							<div className="page-action" onClick={this.showForm}>
								<i className="fa fa-plus"></i>
							</div>
						</div>
					</div>
				</header>
				<div className="main">
					<div className="container-fluid">

						<div id="blog-form" className={formClasses}>
							<div className="panel blog-form">
								<div className="panel-header">
									<div className="panel-action-group">
										<div className="panel-action" onClick={this.cancel}>
											<i className="fa fa-remove"></i>
										</div>
									</div>
								</div>
								<div className="panel-content">
									<form>
										<div className="">
											<div className="form-element">
												<label>Title:</label>
												<input type="text" name="title" value={this.state.blogPost.title} onChange={this.change} />
											</div>
											<div className="form-element intro">
												<label>Intro:</label>
												<textarea name="intro" value={this.state.blogPost.intro} onChange={this.change} />
											</div>
											<div className="form-element post">
												<label className="textarea">Post:</label>
												<textarea name="post" value={this.state.blogPost.post} onChange={this.change} />
											</div>
											<div className="form-element">
												<label>Draft:</label>
												<input type="checkbox" name="draft" checked={this.state.blogPost.draft} onChange={this.change} />
											</div>
											<div className="form-element">
												<label>Tags:</label>
												<input type="text" name="tags" value={this.state.blogPost.tags} onChange={this.change} />
											</div>
										</div>

										<div className="form-element button-group">
											<button onClick={this.save}>
												<i className="fa fa-cloud-upload"></i>
												<span className="title">Save</span>
											</button>
											<button onClick={this.deleteBlogPost}>
												<i className="fa fa-times-circle"></i>
												<span className="title">Delete</span>
											</button>
											<button onClick={this.cancel}>
												<i className="fa fa-close"></i>
												<span className="title">Cancel</span>
											</button>
										</div>
									</form>
								</div>
								<div className="panel-footer"></div>
							</div>
						</div>

						<div id="blog-list" className={listClasses}>
							<ul className="blog-posts">
								{blogPostList}
							</ul>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

export default Blog
