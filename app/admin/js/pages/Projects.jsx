import React from 'react'
import {Link} from "react-router";

class Projects extends React.Component {
	constructor(props) {
		super(props);

		this.getInitialState = this.getInitialState.bind(this);
		this.showForm = this.showForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.change = this.change.bind(this);
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
		this.addImages = this.addImages.bind(this);
		this.changeImages = this.changeImages.bind(this);
		this.removeImage = this.removeImage.bind(this);
		this.getProjects = this.getProjects.bind(this);
		this.deleteProject = this.deleteProject.bind(this);
		this.changeProject = this.changeProject.bind(this);

		// Initialize state
		this.state = this.getInitialState();
		this.getProjects();
	}

	getInitialState() {
		return {
			showForm: false,
			project: {
				id: "0",
				title: "",
				website: "",
				status: "In progress",
				url_title: "",
				description: "",
				technologies: "",
				images: []
			},
			projects: []
		};
	}

	getProjects() {
		var self = this;
		$.ajax({
			url: "/api/projects",
	    	dataType: 'json',
	  	})
	  	.done(function(response) {
			if (response.status == "Success") {
				let projects = response.data;
				for (var i = 0; i < projects.length; i++) {
					if (projects[i].images == null) {
						projects[i].images = [];
					}
				}

				self.setState({projects: projects});
			} else {
				console.log(response.status, ": ", response.message);
			}
	  	})
	  	.fail(function(jqXhr) {
	    	console.log('Failed to get projects', jqXhr);
	  	});
	}

	showForm() {
		this.setState((prevState) => ({
			showForm: true
		}));
	}

	closeForm() {
		this.setState((prevState) => ({
			showForm: false
		}));
		this.getProjects();
	}

	changeProject(project) {
		this.showForm();
		this.setState({project: project});
	}

	addImages(event) {
		event.preventDefault();

		let project = this.state.project;
		let image = {
			image: "",
			thumbnail: ""
		};

		project.images.push(image);
		this.setState({project: project});
	}

	change(event){
		const name = event.target.name;
		const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

		let project = this.state.project;
		project[name] = value;

		this.setState({project: project});
	}

	changeImages(event) {
		const name = event.target.name;
		const index = event.target.getAttribute("data-index");
		let project = this.state.project;

		switch (name) {
			case "image":
				project.images[index].image = event.target.value;
				break;
			case "thumbnail":
				project.images[index].thumbnail = event.target.value;
				break;
		}

		this.setState({project: project});
	}

	removeImage(index) {
		let project = this.state.project;
		if (index > -1) {
		    project.images.splice(index, 1);
		}
		this.setState({project: project});
	}

	deleteProject(event) {
		event.preventDefault();
		let self = this;
		let project = this.state.project;

		$.ajax({
	    	type: 'DELETE',
	    	url: '/api/projects/delete/' + project.id
	  	})
	  	.done(function(data) {
			self.setState(self.getInitialState());
		  	self.closeForm();
	  	})
	  	.fail(function(jqXhr) {
	    	console.log('Failed to delete project:', jqXhr);
	  	});
	}

	save(event) {
		event.preventDefault();
		let self = this;
		let project = this.state.project;
		let saveURL = project.id == 0 ? "/api/projects/create" : "/api/projects/update/" + project.id;

		$.ajax({
	    	type: 'POST',
	    	url: saveURL,
	    	data: JSON.stringify(project)
	  	})
	  	.done(function(data) {
			self.setState(self.getInitialState());
		  	self.closeForm();
	  	})
	  	.fail(function(jqXhr) {
	    	console.log('Failed to save project: ', jqXhr);
	  	});
	}

	cancel(event) {
		event.preventDefault();
		this.setState(this.getInitialState());
		this.closeForm();
	}

	render() {
		let formClasses = this.state.showForm ? "panel-form show-panel" : "panel-form hide-panel";
		let listClasses = !this.state.showForm ? "panel-list show-panel" : "panel-list hide-panel";
		let project = this.state.project;
		let projects = this.state.projects;

		const projectImages = project.images.map((image, index) =>
			<li key={index}>
				<div className="panel-header">
					<div className="panel-action-group">
						<div className="panel-action" onClick={() => this.removeImage(index)}>
							<i className="fa fa-remove"></i>
						</div>
					</div>
				</div>
				<div className="image-preview">
					<img src={image.thumbnail || "/admin/static/img/placeholder.png"} className="img-responsive" />
				</div>
				<div className="image-path">
					<div className="form-element">
						<label>Image:</label>
						<input type="text" name="image" value={image.image} onChange={this.changeImages} data-index={index} />
					</div>

					<div className="form-element">
						<label>Thumbnail:</label>
						<input type="text" name="thumbnail" value={image.thumbnail} onChange={this.changeImages} data-index={index} />
					</div>
				</div>
			</li>
		);

		const projectList = (projects.length === 0) ? null : projects.map((project, index) =>
			<li key={index} className="panel project">
				<div className="panel-header">
					<div className="panel-action-group">
						<div className="panel-action" onClick={() => this.changeProject(project)}>
							<i className="fa fa-pencil-square-o"></i>
						</div>
						{/*
							<div className="panel-action">
								<i className="fa fa-remove"></i>
							</div>
						*/}

					</div>
				</div>
				<div className="panel-content">
					<div className="project-details">
						<h3 onClick={() => this.changeProject(project)}>{project.title}</h3>
						<p>{project.website}</p>
						<p>Technologies: {project.technologies}</p>
						<p>{project.description}</p>
					</div>
					<div className="project-images">
						<ul>
							{project.images.map((image, index) =>
								<li key={index}>
									<img src={image.thumbnail} className="img-responsive" />
								</li>
							)}
						</ul>
					</div>
				</div>

			</li>
		);

		return (
			<div id="projects">
				<header className="page">
					<div className="container-fluid">
						<div className="page-title">
							<h2>
								<i className="fa fa-area-chart" aria-hidden="true"></i>
								Projects
							</h2>
						</div>
						<div className="page-actions">
							<div className="page-action" onClick={this.showForm}>
								<i className="fa fa-plus"></i>
							</div>
						</div>
					</div>
				</header>
				<div className="main">
					<div className="container-fluid">

						<div id="project-form" className={formClasses}>
							<div className="panel project-form">
								<div className="panel-header">
									<div className="panel-action-group">
										<div className="panel-action" onClick={this.cancel}>
											<i className="fa fa-remove"></i>
										</div>
									</div>
								</div>
								<div className="panel-content">
									<form>
										<div className="">
											<div className="form-element">
												<label>Title:</label>
												<input type="text" name="title" value={this.state.project.title} onChange={this.change} />
											</div>
											<div className="form-element">
												<label>Website:</label>
												<input type="text" name="website" value={this.state.project.website} onChange={this.change} />
											</div>
											<div className="form-element">
												<label>Status:</label>
												<select name="status" value={this.state.project.status} onChange={this.change}>
													<option value="In progress">Work in progress</option>
													<option value="Production ready">Finished</option>
												</select>
											</div>
											<div className="form-element">
												<label>Technologies:</label>
												<input type="text" name="technologies" value={this.state.project.technologies} onChange={this.change} />
											</div>
											<div className="form-element">
												<label className="textarea">Description:</label>
												<textarea name="description" value={this.state.project.description} onChange={this.change} />
											</div>
										</div>
										<div className="">
											<div className="form-element">
												<button onClick={this.addImages}>
													<i className="fa fa-picture-o"></i>
													<span className="title">Add Images</span>
												</button>
											</div>
											<div className="form-element">
												<ul className="images">{projectImages}</ul>
											</div>
										</div>
										<div className="form-element button-group">
											<button onClick={this.save}>
												<i className="fa fa-cloud-upload"></i>
												<span className="title">Save</span>
											</button>
											<button onClick={this.deleteProject}>
												<i className="fa fa-times-circle"></i>
												<span className="title">Delete</span>
											</button>
											<button onClick={this.cancel}>
												<i className="fa fa-close"></i>
												<span className="title">Cancel</span>
											</button>
										</div>
									</form>
								</div>
								<div className="panel-footer"></div>
							</div>
						</div>

						<div id="project-list" className={listClasses}>
							<ul className="projects">
								{projectList}
							</ul>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

export default Projects
