import React from 'react'
import {Link} from "react-router";

class Dashboard extends React.Component {
	render() {
		return (
			<div id="dashboard">
				<header className="page">
					<div className="container-fluid">
						<div className="page-title">
							<h2 className="">
								<i className="fa fa-dashboard" aria-hidden="true"></i>
								Dashboard
							</h2>
						</div>
					</div>
				</header>
				<div className="main">
					<div className="container-fluid">
						<div className="panel-container">
							<div className="panel projects">
								<div className="panel-header"></div>
								<div className="panel-content text-center">
									<Link to="/admin/projects" activeClassName="">
										<i className="fa fa-area-chart" aria-hidden="true"></i>
										<span className="text">Projects</span>
									</Link>
								</div>
							</div>

							<div className="panel blog">
								<div className="panel-header"></div>
								<div className="panel-content text-center">
									<Link to={"/admin/blog"} activeClassName="">
										<i className="fa fa-id-card-o" aria-hidden="true"></i>
										<span className="text">Blog</span>
									</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Dashboard
