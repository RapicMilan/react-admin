import React from 'react'
import Header from './Header'
import Sidebar from './Sidebar'
import Content from './Content'

class Shell extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
			sidebarOpen: true
		};
		this.handleViewSidebar = this.handleViewSidebar.bind(this);
	}

	handleViewSidebar() {
		//this.setState({sidebarOpen: !this.state.sidebarOpen});
		this.setState((prevState) => ({
	      	sidebarOpen: !prevState.sidebarOpen
	    }));
	}

	render() {
		var shellClass = this.state.sidebarOpen ? '' : 'sidebar-hide';
		return (
			<div className={shellClass}>
				<Header isOpen={this.state.sidebarOpen} onClick={this.handleViewSidebar} />
				<Sidebar isOpen={this.state.sidebarOpen} toggleSidebar={this.handleViewSidebar}/>
				<Content isOpen={this.state.sidebarOpen} content={this.props.children} />
			</div>
		);
	}
}

export default Shell
