import React from 'react'
import {Link} from "react-router";

class Sidebar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			settingsSubmenuOpen: false
		};
		this.settingsSubmenuToggle = this.settingsSubmenuToggle.bind(this);
	}

	settingsSubmenuToggle() {
		this.setState((prevState) => ({
	      	settingsSubmenuOpen: !prevState.settingsSubmenuOpen
	    }));
	}

	render() {
		let sidebarClass = 'sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures';
		let nanoClass = this.props.isOpen ? 'nano has-scrollbar' : 'nano';

		let settingsSidebarCollapseIconClass = this.state.projectsSubmenuOpen ? "sidebar-collapse-icon fa fa-angle-up" : "sidebar-collapse-icon fa fa-angle-down";
		let settingsSubmenuOpenClass = this.state.settingsSubmenuOpen ? "open" : "";
		let settingsSubmenuShowClass = this.state.settingsSubmenuOpen ? "show" : "hide";

		return (
			<div className={sidebarClass}>
		      	<div className={nanoClass}>
					<div className="nano-content">
						<ul>
							<li>
								<Link to={"/admin/dashboard"} activeClassName="active">
									<i className="fa fa-dashboard" aria-hidden="true"></i>
									Dashboard
								</Link>
							</li>
							<li>
								<Link to="/admin/projects" activeClassName="active">
									<i className="fa fa-area-chart" aria-hidden="true"></i>
									Projects
								</Link>
							</li>
							<li>
								<Link to={"/admin/blog"} activeClassName="active">
									<i className="fa fa-id-card-o" aria-hidden="true"></i>
									Blog
								</Link>
							</li>
							<li className={settingsSubmenuOpenClass}>
								<Link to="/admin" activeClassName="" className="sidebar-sub-toggle" onClick={this.settingsSubmenuToggle}>
									<i className="fa fa-gears" aria-hidden="true"></i>
									Settings
									<span className={settingsSidebarCollapseIconClass} aria-hidden="true"></span>
								</Link>
								<ul className={settingsSubmenuShowClass}>
									<li>
										<Link to="/admin" activeClassName="active">Settings 1</Link>
									</li>
									<li>
										<Link to="/admin" activeClassName="active">Settings 2</Link>
									</li>
									<li>
										<Link to="/admin" activeClassName="active">Settings 3</Link>
									</li>
								</ul>
							</li>
							<li>
								<Link to={"/admin"} activeClassName="">
									<i className="fa fa-users" aria-hidden="true"></i>
									Users
								</Link>
							</li>
						</ul>
					</div>
				</div>
	    	</div>
		);
	}
}

export default Sidebar
