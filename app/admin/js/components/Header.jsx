import React from 'react'

class Header extends React.Component {
	render() {
		var sidebarClass = this.props.isOpen ? 'hamburger sidebar-toggle' : 'hamburger sidebar-toggle is-active';
		return (
			<header className="header">
				<div className="header-section-left">
					<div className="logo"><img src="/admin/static/img/logo/logo.png" alt="" /><span>ADMIN</span></div>
					<div className={sidebarClass} onClick={this.props.onClick}>
						<span className="line"></span>
						<span className="line"></span>
						<span className="line"></span>
					</div>
				</div>
				<div className="header-section-rigth">

				</div>
			</header>
		);
	}
}

export default Header
