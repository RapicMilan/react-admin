import React from 'react'

class Content extends React.Component {
	render() {
		return (
			<div className="content-wrap">
				{this.props.content}
			</div>
		);
	}
}

export default Content
