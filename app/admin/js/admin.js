import React from 'react'
import ReactDOM from 'react-dom'
import {Router, Route, browserHistory, IndexRoute} from "react-router";
import Shell from './components/Shell'
import Dashboard from './pages/Dashboard'
import Projects from './pages/Projects'
import Blog from './pages/Blog'

class App extends React.Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path={"/admin"} component={Shell} >
                    <IndexRoute component={Dashboard} />
					<Route path={"/admin/dashboard"} component={Dashboard} />
					<Route path={"/admin/projects"} component={Projects} />
					<Route path={"/admin/blog"} component={Blog} />
                </Route>


            </Router>
        );
    }
}


ReactDOM.render(<App />, document.getElementById("application"))
