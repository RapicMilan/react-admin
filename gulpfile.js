/** **************************************************************** **

    @Project:   Build Automation Script
	@Author:    Rapić Milan
	@Website:   www.rapicmilan.com

** **************************************************************** **/


var gulp        = require('gulp');
var requireDir = require('require-dir');
requireDir('./gulptasks');



gulp.task('watch', function() {
  gulp.watch('app/admin/js/**/*.{js,jsx}', ['admin-js']);
  gulp.watch('app/admin/css/**/*.css', ['admin-css']);
  gulp.watch('app/admin/sass/**/*.scss', ['admin-css']);

  gulp.watch('app/contact/js/**/*.{js,jsx}', ['contact-js']);
  gulp.watch('app/contact/css/**/*.css', ['contact-css']);
  gulp.watch('app/contact/sass/**/*.scss', ['contact-css']);

});


gulp.task('default', ['build-admin', 'build-contact', 'watch']);

/** **************************************************************** **/
